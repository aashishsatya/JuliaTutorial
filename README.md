

```julia
2 + 3 # yep, REPL works, these are comments by the way
```




    5




```julia
# declare variables

five = 2 + 3

# you have the usual Int, Float, Char, Boolean etc as basic data types
```




    5




```julia
print(five) # use print to print (duh?)
```

    5


```julia
# if statements -- notice if, else, elseif, end (don't forget the end in the end)

if five == 4
    print("Four")
elseif five == 5
    print("Five")
else
    print("None of the above")
end
```

    Five


```julia
# now, loops

# for loops are almost as they are in Python
# again, don't forget the 'end'

for i in 1: 5 # 1: 5 gives an iterator for numbers from one to five (inclusive)
    println("Hello World!") # println adds a newline at the end
end
```

    Hello World!
    Hello World!
    Hello World!
    Hello World!
    Hello World!



```julia
# now while loop

i = 0
while i < 5
    println("Hello World!")
    i += 1
end
```

    Hello World!
    Hello World!
    Hello World!
    Hello World!
    Hello World!



```julia
# functions

function square(n)
    return n * n
end

square(2)
```




    4




```julia
# you can make the argument type of the function explicit
# THIS IS RECOMMENDED TO SPEED UP COMPUTATION, AS THEN JULIA DOESN'T HAVE TO DO TYPE INFERENCE

function int_square(n :: Int64)
    return n * n
end

int_square(2)
```




    4




```julia
# now arrays

# explicit declaration:

array_init = [1, 2, 3, 4, 5]
```




    5-element Array{Int64,1}:
     1
     2
     3
     4
     5




```julia
# implicit declaration

array_uninit = Array{Int, 1}() # format is Array{<type>, <dimension>}(<number of elements>)

# array_uninit is a one-dimensional array with zero entries of type Integer (note the parentheses in the end)
```




    0-element Array{Int64,1}




```julia
# add elements into the array

push!(array_uninit, 1, 2) # ! at the end of the function name indicates that the input parameters are viable to change
```




    2-element Array{Int64,1}:
     1
     2




```julia
# arrays are mutable

array_uninit[1] = 3 # array indexing begins at one!
array_uninit
```




    2-element Array{Int64,1}:
     3
     2




```julia
# right, we know enough Julia to implement a recursive binary search
# you're advised to try this one on your own

function binary_search(array, key, low, high)
    
    if low > high
        return -1
    end
    
    mid = div(high + low, 2)
    if array[mid] == key
        return mid
    elseif array[mid] < key
        low = mid + 1
    else
        high = mid - 1
    end
    
    return binary_search(array, key, low, high)
    
end

array = 1: 10
binary_search(array, 3, 1, 10)
```




    3




```julia
# perhaps you'd like a dictionary

# format for initializing is Dict{<type of key>, <type of value>}() -- note the parens!

my_dict = Dict{Int, Int}()
my_dict[1] = 2
my_dict[2] = 4
my_dict
```




    Dict{Int64,Int64} with 2 entries:
      2 => 4
      1 => 2




```julia
# user defined data types (go figure sets, tuples on your own)

type Person
    name :: String
    age :: Int64
end

# let's write a function that sorts people in the ascending order of their age

function person_sort!(person_array)
    
    # bubble sort, coz I'm lazy
    
    for i in 1: length(person_array)
        for j in 1: length(person_array) - 1
            if person_array[j].age > person_array[j + 1].age
                temp = person_array[j]
                person_array[j] = person_array[j + 1]
                person_array[j + 1] = temp
            end
        end
    end
    
end

person_arr = Array{Person, 1}()
push!(person_arr, Person("A", 10))
push!(person_arr, Person("B", 5))
push!(person_arr, Person("C", 1))
```




    3-element Array{Person,1}:
     Person("A", 10)
     Person("B", 5) 
     Person("C", 1) 




```julia
person_sort!(person_arr)
```


```julia
person_arr
```




    3-element Array{Person,1}:
     Person("C", 1) 
     Person("B", 5) 
     Person("A", 10)




```julia
# This marks the end of the simple (5-minute) Julia tutorial
# Next section covers multi-dimensional arrays and parallelization (covering @parallel and pmap)
```


```julia
# let's write the Julia code for matrix multiplication

# define the matrix
# there are a plethora of ways to do it, see https://en.wikibooks.org/wiki/Introducing_Julia/Arrays_and_tuples#Creating_2D_arrays_and_matrices

A = rand(2, 3) 
B = rand(3, 2) 

function mat_mult(matA :: Array{Float64, 2}, matB :: Array{Float64, 2})
    
    # suppose A is m x n and B is n x p
    
    m = size(matA, 1) # size of matrix A along dimension 1
    n = size(matA, 2)
    p = size(matB, 2)
    
    mat_prod = zeros(Float64, m, p) # create an n x p matrix with zero entries
    
    # rest is straight forward
    
    for i in 1: m
        for j in 1: p
            for k in 1: n
                mat_prod[i, j] += matA[i, k] * matB[k, j] # [i, j] for indexing the element in ith row, jth column
            end
        end
    end
    
    return mat_prod
    
end

mat_mult(A, B)
```




    2×2 Array{Float64,2}:
     1.1548    0.727594
     0.848721  0.480301




```julia
# now parallelization

# NOTE: YOU NEED TO START JULIA WITH MORE THAN ONE THREAD WHEN RUNNING PROGRAMS USING PARALLEL PROCESSES
# This can be done by running 'julia -p <number of processes>' while starting Julia, or
# by running 'addprocs(<number of processes>)' in the Julia prompt

addprocs(2)

# below is the serial code to add a billion random numbers

function serial_add()
    s = 0.0
    for i = 1:1000000000
        s = s + randn()
    end
    return s
end

# (in Rick's voice, from Rick and Morty) alright, let's write some parallel code!

function parallel_add()
    
    # format is
    # return (or <variable> =) @parallel (<operator to combine the results>) <iterate over what>
    # Julia takes care of assigning jobs to threads on its own
    # isn't she sweet
    
    return @parallel (+) for i = 1:1000000000
        randn()
    end
    
end
```




    parallel_add (generic function with 1 method)




```julia
# now pmap

# if you're calling another function in the code you parallelize you must use the @everywhere tag:

@everywhere function square(n)
    return n * n
end

# now pmap is easy

squares = pmap(square, 1:10)
```




    10-element Array{Int64,1}:
       1
       4
       9
      16
      25
      36
      49
      64
      81
     100




```julia
# end of tutorial, thank you!
```
